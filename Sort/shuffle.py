from random import randrange
 
def knuth_shuffle(x):
    for i in range(len(x)-1, 0, -1):
        j = randrange(i + 1)
        x[i], x[j] = x[j], x[i]
 
x = list(range(10))
knuth_shuffle(x)
print("shuffled:", x)

from random import randrange
def sattoloCycle(items):
	for i in range(len(items) - 1, 0, -1):
		j = randrange(i)  # 0 <= j <= i-1
		items[j], items[i] = items[i], items[j]
 
  # Tests
for _ in range(10):
	lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	sattoloCycle(lst)
	print(lst)
