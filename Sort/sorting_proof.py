def insertion(seq):
	for n in range(1, len(seq)):
        item = seq[n]
        hole = n
        while hole > 0 and seq[hole - 1] > item:
            seq[hole] = seq[hole - 1]
            hole = hole - 1
        seq[hole] = item
    return seq

def selection(seq):
	for i in range(0, len(seq)):
        iMin = i
        for j in range(i+1, len(seq)):
            if seq[iMin] > seq[j]:
                iMin = j
        if i != iMin:
            seq[i], seq[iMin] = seq[iMin], seq[i]

    return seq

def count(seq):
    n = len(seq)
    sum_ins = 0
    sum_sel = 0
    for i in range(100):
        t1 = time.clock()
        insertion(seq)
        t2 = time.clock()
        delta1 = (t2 - t1)*1000
        sum_ins+=delta1
    for i in range(100):   
       t3 = time.clock()
       cocktail_sort(seq)
       t4 = time.clock()
       delta2 = (t4 - t3)*1000
       sum_sel+=delta2

    return n, sum_sel/100, sum_ins/100

def print_table():    
    table_head1 = '      |    Время( в мс )   |'
    table_head2 = '|  n  | Bubble  | Cocktail |'
    print('-'*28)
    print(table_head1)
    print('-'*28)
    print(table_head2)
    print('-'*28)

arr1 = [random.randint(1,100) for i in range(10)]
arr2 = [random.randint(1,100) for i in range(100)]
arr3 = [random.randint(1,500) for i in range(1000)]
arr4 = [random.randint(1,1000) for i in range(10000)]
arrays = [arr1, arr2, arr3, arr4]
print_table()
    for i in range(len(arrays)):
        n, d1, d2 = count(arrays[i])
        print('|{:5d}|{:9.3f}|{:10.4f}|'.format(n, d1, d2))
        print('-'*28)
    
    
