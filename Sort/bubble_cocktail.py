# Покасова Анастасия, ИУ7-21. Сортировка массива
# методами пузырька и перемешивания с выводом времени,
# затраченного на сортировку.

import time
import random

# Пузырьковая сортировка
def bubble_sort(seq):
    l = len(seq)
    for i in range(l):
        for n in range(1, l-i):
            if seq[n] < seq[n-1]:
                seq[n-1], seq[n] = seq[n], seq[n-1]
    return seq

# Шейкерная сортировка (перемешиванием)
def cocktail_sort(seq):
    l_bound = -1
    u_bound = len(seq) - 1
    swapped = True
    while swapped:
        swapped = False
        l_bound += 1
        for i in range(l_bound, u_bound):
            if seq[i] > seq[i + 1]:
                seq[i], seq[i + 1] = seq[i + 1], seq[i]
                swapped = True
        if not swapped:
            break
        swapped = False
        u_bound -= 1
        for i in range(u_bound, l_bound, -1):
            if seq[i] < seq[i - 1]:
                seq[i], seq[i - 1] = seq[i - 1], seq[i]
                swapped = True
    return seq

# Расчет времени выполнения алгоритма
def count(seq):
    n = len(seq)
    t1 = time.clock()
    bubble_sort(seq)
    t2 = time.clock()
    delta1 = (t2 - t1)*1000
    t3 = time.clock()
    cocktail_sort(seq)
    t4 = time.clock()
    delta2 = (t4 - t3)*1000

    return n, delta1, delta2

# Печать результатов
def print_table():    
    table_head1 = '      |    Время( в мс )   |'
    table_head2 = '|  n  | Bubble  | Cocktail |'
    print('-'*28)
    print(table_head1)
    print('-'*28)
    print(table_head2)
    print('-'*28)

pos = ['y', 'н']
neg = ['n', 'т', '']

# Получение исходных данных
print('Ввести собственный массив?(y/n)')
i = input()

if i in pos:
    print('Задайте элементы массива (числа) через пробел')
    seq = [float(x) for x in input().split()]
    n, d1, d2 = count(seq)
    seq = cocktail_sort(seq)
    print()
    print('Отсортированный массив')
    for i in seq:
        print(i, end=' ')
    print()
    print_table()
    print('|{:^5d}|{:9.4f}|{:10.4f}|'.format(n, d1, d2))
    print('-'*28)
    
elif i in neg:
    # Заготовленные массивы 
    arr1 = [random.randint(1,100) for i in range(10)]
    arr2 = [random.randint(1,100) for i in range(100)]
    arr3 = [random.randint(1,500) for i in range(1000)]
    arr4 = [random.randint(1,1000) for i in range(10000)]
    arrays = [arr1, arr2, arr3, arr4]
    print_table()
    for i in range(len(arrays)):
        n, d1, d2 = count(arrays[i])
        print('|{:5d}|{:9.3f}|{:10.4f}|'.format(n, d1, d2))
        print('-'*28)

