#1 bubble sorting
def bubble(seq):
	L = len(seq)
    for i in range(L):
        for n in range(1, L - i):
            if seq[n] < seq[n - 1]:
                seq[n - 1], seq[n] = seq[n], seq[n - 1]
    return seq

#2 cocktail(shaker) sorting
def cocktail(seq):
	lower_bound = -1
    upper_bound = len(seq) - 1
    swapped = True
    while swapped:
        swapped = False
        lower_bound += 1
        for i in range(lower_bound, upper_bound):
            if seq[i] > seq[i + 1]:
                seq[i], seq[i + 1] = seq[i + 1], seq[i]
                swapped = True
        if not swapped:
            break
        swapped = False
        upper_bound -= 1
        for i in range(upper_bound, lower_bound, -1):
            if seq[i] < seq[i - 1]:
                seq[i], seq[i - 1] = seq[i - 1], seq[i]
                swapped = True
    return seq

#3 merge sorting
def merge(left, right):
    result = []
    n, m = 0, 0
    while n < len(left) and m < len(right):
    if left[n] <= right[m]:
         result += [left[n]]
        n += 1
    else:
        result += [right[m]]
        m += 1
    for i in range(n, len(left)):
        result += [left[i]]
    for i in range(m, len(right)):
        result += [right[i]]

    return result

def sort(seq):
    if len(seq) <= 1:
        return seq
    middle = int(len(seq) / 2)
    left = []
    right = []
    for i in range(middle):
        left += [seq[i]]
    for i in range(middle, len(seq)):
        right += [seq[i]]

    left = sort(left)
    right = sort(right)
    return merge(left, right)


#4 shell sorting
def shell(seq):
	gaps = [x for x in range(len(seq) // 2, 0, -1)]

    for gap in gaps:
        for i in range(gap, len(seq)):
            temp = seq[i]
            j = i
            while j >= gap and seq[j - gap] > temp:
                seq[j] = seq[j - gap]
                j -= gap
            seq[j] = temp
    return seq


#5 quick sorting
def quicksort(seq):
    if len(seq) <= 1:
        return seq
    else:
        pivot = seq[0]
        left, right = [], []
        temp = []
        for i in range(1, len(seq)):
            temp += [seq[i]]
        for i in range(len(temp)):
            if temp[i] < pivot:
                left += [temp[i]]
            else:
                right += [temp[i]]
        return quicksort(left) + [pivot] + quicksort(right)

#6 insertion sorting
def insertion(seq):
	for n in range(1, len(seq)):
        item = seq[n]
        hole = n
        while hole > 0 and seq[hole - 1] > item:
            seq[hole] = seq[hole - 1]
            hole = hole - 1
        seq[hole] = item
    return seq

#7 selection sort
def selection(seq):
	for i in range(0, len(seq)):
        iMin = i
        for j in range(i+1, len(seq)):
            if seq[iMin] > seq[j]:
                iMin = j
        if i != iMin:
            seq[i], seq[iMin] = seq[iMin], seq[i]

    return seq

#8 heap sorting
def max_heapify(seq, i, n):
    l = 2 * i + 1
    r = 2 * i + 2

    if l <= n and seq[l] > seq[i]:
        largest = l
    else:
        largest = i
    if r <= n and seq[r] > seq[largest]:
        largest = r

    if largest != i:
        seq[i], seq[largest] = seq[largest], seq[i]
        max_heapify(seq, largest, n)

def build_heap(seq):
    n = len(seq) - 1
    for i in range(n//2, -1, -1):
        max_heapify(seq, i, n)


def sort(seq):
    build_heap(seq)
    heap_size = len(seq) - 1
    for x in range(heap_size, 0, -1):
        seq[0], seq[x] = seq[x], seq[0]
        heap_size = heap_size - 1
        max_heapify(seq, 0, heap_size)

    return seq

#9 comb sorting
def comb_sort(seq):
    gap = len(seq)
    swap = True

    while gap > 1 or swap:
        gap = max(1, int(gap / 1.25))
        swap = False
        for i in range(len(seq) - gap):
            if seq[i] > seq[i + gap]:
                seq[i], seq[i + gap] = seq[i + gap], seq[i]
                swap = True
    return seq

#10 insertion with barrier
def inserion_with_barrier(array):
    back=array[0]
    for i in range(2, len(array)):
        if array[i - 1] > array[i]: 
            array[0] = array[i]
            j = i - 1
            while array[j] > array[0]:
                array[j + 1] = array[j]
                j = j - 1
            array[j + 1] = array[0]
    array[0]=back
    i=1
    while array[i]<back:
        array[i-1]=array[i]
        i+=1  
    array[i-1]=back

    return array

#11 insertion sort with binary search
def insertion_sort_bin(seq):
    for i in range(1, len(seq)):
        key = seq[i]
        low, up = 0, i
        while up > low:
            middle = (low + up) // 2
            if seq[middle] < key:
                low = middle + 1              
            else:
                up = middle
        seq[:] = seq[:low] + [key] + seq[low:i] + seq[i + 1:]
    return seq


#12 radix sort
from math import log
def getDigit(num, base, digit_num):
    # pulls the selected digit
    return (num // base ** digit_num) % base  
def makeBlanks(size):
    # create a list of empty lists to hold the split by digit
    return [ [] for i in range(size) ]  
def split(a_list, base, digit_num):
    buckets = makeBlanks(base)
    for num in a_list:
        # append the number to the list selected by the digit
        buckets[getDigit(num, base, digit_num)].append(num)  
    return buckets
# concatenate the lists back in order for the next step
def merge(a_list):
    new_list = []
    for sublist in a_list:
       new_list.extend(sublist)
    return new_list
def maxAbs(a_list):
    # largest abs value element of a list
    return max(abs(num) for num in a_list)
def split_by_sign(a_list):
    # splits values by sign - negative values go to the first bucket,
    # non-negative ones into the second
    buckets = [[], []]
    for num in a_list:
        if num < 0:
            buckets[0].append(num)
        else:
            buckets[1].append(num)
    return buckets
def radixSort(a_list, base):
    # there are as many passes as there are digits in the longest number
    passes = int(round(log(maxAbs(a_list), base)) + 1) 
    new_list = list(a_list)
    for digit_num in range(passes):
        new_list = merge(split(new_list, base, digit_num))
    return merge(split_by_sign(new_list))
