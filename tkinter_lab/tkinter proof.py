# Покасова Анастасия, ИУ7-21

from tkinter import *
from math import sqrt
root = Tk()
draw_pole = Canvas(root, width = 800, height = 600, bg = "white")

def lies_inside(circle,radius,point):
    x1, y1 = point[0], point[1]
    x2, y2 = circle[0], circle[1]
    inside = False
    length = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))
    if length < radius:
        inside = True
    return inside
        
R = 3

points = []
print("\nВведите координаты 10 точек в формате x y:")
for i in range(10):
    x,y = map(float,input().split())
    points += [[x,y]]

circles = [[1,2],[-5,-5],[6,7]]

min_x = max_x = points[0][0]
min_y = max_y = points[0][1]

for point in points:
    max_x = max(max_x, point[0])
    min_x = min(min_x, point[0])
    max_y = max(max_y, point[1])
    min_y = min(min_y, point[1])

for circle in circles:
    max_x = max(max_x, circle[0])
    min_x = min(min_x, circle[0])
    max_y = max(max_y, circle[1])
    min_y = min(min_y, circle[1])

scale = min(500/(max_y - min_y), 700/(max_x - min_x))
disp_x = 50 + round((700 - (max_x - min_x)*scale)/2)
disp_y = 550 - round((500 - (max_y - min_y)*scale)/2)

x = round(disp_x - min_x*scale)
y = round(disp_y + min_y*scale)
draw_pole.create_line(0, y, 800, y, width=2, fill="grey", arrow=LAST)
draw_pole.create_line(x, 600, x, 0, width=2, fill="grey", arrow=LAST)

draw_pole.create_text(x + 8, 9, text = "y",\
          font="Arial 8", justify=CENTER, fill="green")
draw_pole.create_text(790, y - 9, text = "x",\
          font="Arial 8", justify=CENTER, fill="green")

R = round(R * scale)

max_inside = 0
max_index = 0
for i in range(3):
    points_inside = 0
    for j in range(10):
        inside = lies_inside(circles[i],R,points[j])
        if inside == True:
            points_inside +=1
    if points_inside > max_inside:
        max_inside = points_inside
        max_index = i+1

        
print('Больше всего точек попадает в окружность', max_index)
print('с центром в точке', circles[max_index-1])
for point in points:
    x = round(disp_x + (point[0] - min_x)*scale)
    y = round(disp_y - (point[1] - min_y)*scale)
    draw_pole.create_oval(x-2, y-2, x+2, y+2, fill="black")
    draw_pole.create_text(x, y - 13,text="({:};{:})".format(point[0], point[1]),\
          font="Arial 8", justify=CENTER, fill="blue")


for circle in circles:
    x = round(disp_x + (circle[0] - min_x)*scale)
    y = round(disp_y - (circle[1] - min_y)*scale)
    draw_pole.create_oval(x - R, y - R, x + R, y + R, outline = "red")
    draw_pole.create_oval(x - 1, y - 1, x + 1, y + 1, fill = "red")
    draw_pole.create_text(x, y - 13, text="({:};{:})".format(circle[0],\
                          circle[1]), font="Arial 8", justify=CENTER, \
                          fill="green")
draw_pole.pack()
root.mainloop()
