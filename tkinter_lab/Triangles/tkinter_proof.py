from tkinter import *
def form_circles():
    centers=[]
    radiuses = []
    print('input number of circles')
    n = int(input())
    print('input coordinate of center of the circle and its radius')
    for i in range(n):
        x,y, radius = map(float, input().split())
        centers += [[x,y]]
        radiuses += [radius]
    return centers, radiuses

centers, radiuses = form_circles()
print(centers)
print(radiuses)

root = Tk()
c = Canvas(root, width = 800, height = 600, bg = '#ffffff')
screen_scale = min(300/3, 500/5)
disp_x = 10 + round((700 - 3*screen_scale)/2)
disp_y = 610 - round((500 - 3*screen_scale)/2) 

for i in range(len(centers)):
    print(centers[i])
    x = round(disp_x + (centers[0][0])*screen_scale)
    y = round(disp_y - (centers[0][1])*screen_scale)
    c.create_oval(x-40, y-40, x+40, y+40, fill="", outline='black')

c.pack()
root.mainloop()
