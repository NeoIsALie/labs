# Покасова Анастасия, ИУ7-21, вариант 13.
# Задано множество точек на плоскости.
# Найти треугольник, построенный на этих точках, с наибольшим углом.
# Дать графическое изображение.

'''
Список используемых функций:
    main - тело программы
    form_points - задание массива точек
    build_vector - построение вектора по точкам
    check_triangle - проверка треугольника на существование
    find_arc - вычисление значений угла в треугольнике
    draw_triangle - рисование треугольника

Список основных переменных:
    points - массив точек
    max_arc - наибольший угол
    max_arc_tr_index - номер треуольника с наибольшим углом
'''

from math import sqrt, acos
from tkinter import *

# Задание массива точек 
def form_points():
    print("Number of points: ", end='')
    try:
        n = int(input())
        while n < 3:
            print('At least 3 points needed to form a triangle')
            print("Number of points: ", end='')
            n = int(input())
        print('input points by their coordinates (x and y)')
        points = []
        try:
            for i in range(n):
    	        x, y = map(float,input().split())
    	        points += [[x,y]]
        except ValueError:
    	    print('Input coordinates correctly')
        else:
            return points
    except ValueError:
        print('Incorrect data')

    
# Построение вектора (необходимо для расчета скалярного произведения)
def build_vector(p1,p2,p3):
    # координаты векторов
    v1_x = p2[0] - p1[0]
    v1_y = p2[1] - p1[1]
    v1_z = 0

    v2_x = p3[0] - p1[0]
    v2_y = p3[1] - p1[1]
    v2_z = 0
    
    return v1_x, v1_y, v1_z, v2_x, v2_y, v2_z

# Проверка треугольника на существование
def check_existence(p1,p2,p3):
	c1, c2, c3 = p1, p2, p3
	a, b, c, d, e, f = build_vector(c1,c2,c3)
	tr_exists = False
	cr_p = (b*f - c*e) - (a*f - c*d) + (a*e - b*d)
	if cr_p != 0:
	    tr_exists = True

	return tr_exists
	
# Нахождение наибольшего угла в треугольнике
def find_max_arc(p1,p2,p3):
    
    v1_x = p2[0] - p1[0]
    v1_y = p2[1] - p1[1]
    v2_x = p1[0] - p3[0]
    v2_y = p1[1] - p3[1]
    v3_x = p2[0] - p3[0]
    v3_y = p2[1] - p3[1]
    
    arc_c_1 = (v1_x*v2_x+v1_y*v2_y)/\
              (sqrt(pow(v1_x,2)+pow(v1_y,2))*sqrt(pow(v2_x,2)+pow(v2_y,2)))
    arc_c_2 = (v2_x*v3_x+v2_y*v3_y)/\
              (sqrt(pow(v2_x,2)+pow(v2_y,2))*sqrt(pow(v3_x,2)+pow(v3_y,2)))
    arc_c_3 = (v1_x*v3_x+v1_y*v3_y)/\
              (sqrt(pow(v1_x,2)+pow(v1_y,2))*sqrt(pow(v3_x,2)+pow(v3_y,2)))
    max_arc_cos = min(arc_c_1,arc_c_2,arc_c_3)
    
    max_arc = acos(max_arc_cos)
    return max_arc

# Отрисовка треугольника на холсте
def draw_triangle(p1,p2,p3,c,c_w,c_h,max_x,max_y,min_x,min_y,scale):
    disp_x = 60 + round((600 - (max_x - min_x)*scale)/2)
    disp_y = 560 - round((400 - (max_y - min_y)*scale)/2)
    
    x0 = round(disp_x + (p1[0] - min_x)*scale)
    y0 = round(disp_y - (p1[1] - min_y)*scale)
    x1 = round(disp_x + (p2[0] - min_x)*scale)
    y1 = round(disp_y - (p2[1] - min_y)*scale)
    x2 = round(disp_x + (p3[0] - min_x)*scale)
    y2 = round(disp_y - (p3[1] - min_y)*scale)
    c.create_polygon(x0,y0,x1,y1,x2,y2,outline='cyan',fill='',width=2)
    c.create_text(x0, y0 - 13,text="({:};{:})".format(p1[0],p1[1]),\
          font="Arial 8", justify=CENTER, fill="blue")
    c.create_text(x1, y1 - 13,text="({:};{:})".format(p2[0],p2[1]),\
          font="Arial 8", justify=CENTER, fill="blue")
    c.create_text(x2, y2 - 13,text="({:};{:})".format(p3[0],p3[1]),\
          font="Arial 8", justify=CENTER, fill="blue")

# Основная функция (содержит основной рабочий цикл)
def main():


    root = Tk()
    c = Canvas(root, width = 800, height = 600, bg = '#ffffff')

    points = form_points()
    if points == None:
        return print('No triangles can be built')
    max_arc = 0
    max_arc_tr_index = 0
    tr_index = 0
    
    # определение смещения и коэффициента растяжения
    # (количества пикселей в пункте)
    min_x = max_x = points[0][0]
    min_y = max_y = points[0][1]

    for point in points:
        max_x = max(max_x, point[0])
        min_x = min(min_x, point[0])
        max_y = max(max_y, point[1])
        min_y = min(min_y, point[1])
    try:    
        screen_scale = min(300/(max_y - min_y), 500/(max_x - min_x))
        disp_x = 10 + round((700 - (max_x - min_x)*screen_scale)/2)
        disp_y = 610 - round((500 - (max_y - min_y)*screen_scale)/2)
    
        # Отрисовка координатных осей
        x = round(disp_x - min_x*screen_scale)
        y = round(disp_y + min_y*screen_scale)
        c.create_line(0, y, 800, y, width=2, fill="grey", arrow=LAST)
        c.create_line(x, 600, x, 0, width=2, fill="grey", arrow=LAST)
    except ZeroDivisionError:
        print('Sorry, wrong data')
    
    # Проход по массиву точек; построение треугольников    
    for i in range(len(points)-2):
    	for j in range(i, len(points)-1):
    		for k in range(j, len(points)):
    			existence = check_existence(points[i],points[j], points[k])
    			if existence == True:
    				tr_index += 1
    				triangle = [points[i],points[j],points[k]]
    				draw_triangle(points[i],points[j],points[k],
                                              c,800,600,max_x,max_y,
                                              min_x,min_y,screen_scale)
    				arc = find_max_arc(points[i],points[j],points[k])
    				if arc > max_arc:
    					max_arc = arc
    					max_arc_tr_index = tr_index
    					max_tr = triangle
    			else:
    				continue
    try:
        # выделение треугольника с максимальным углом
        x0 = round(disp_x + (max_tr[0][0] - min_x)*screen_scale)
        y0 = round(disp_y - (max_tr[0][1] - min_y)*screen_scale)
        x1 = round(disp_x + (max_tr[1][0] - min_x)*screen_scale)
        y1 = round(disp_y - (max_tr[1][1] - min_y)*screen_scale)
        x2 = round(disp_x + (max_tr[2][0] - min_x)*screen_scale)
        y2 = round(disp_y - (max_tr[2][1] - min_y)*screen_scale)
        print('Maximum angle value: ',end = ' ')
        print('{:6.4f}'.format(max_arc),end=' ')
        print('radians')
        c.create_polygon(x0,y0,x1,y1,x2,y2,outline='black',fill='',width=2)
        c.pack()
        root.mainloop()
    except UnboundLocalError:
            print('No triangles can be built')


main()

    








