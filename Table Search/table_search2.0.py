# Покасова Анастасия, ИУ7-21
# Однофайловая база данных. Использован формат .csv

menu = '''\
0 - выход
1 - создание новой базы
2 - добавление записи
3 - поиск по записи/её части
4 - вывод таблицы
'''


base_name = 'Base.csv'
base_open = False

# Открытие любой существующей базы
def open_base():
    global base
    global base_name
    global base_open
    
    if not base_open:
        print('Нет открытой базы; открыть',base_name,'(y) или другую (n)?')
        if input() == 'n':
            base_name = input('Введите имя базы для открытия:')+'.csv'
        base = open(base_name,'a+')
        base_open = True
    else:
        print('База ',base_name)

choice = ''
while True:
    print(menu)
    choice = input('Введите номер выбранного пункта меню: ')
    print()
    
# Создание базы(файла)
    if choice == '1':
        if base_open:
            base.close()
            base_open = False
        base_name = input('Введите имя новой базы (без расширения): ')+'.csv'
        base = open(base_name,'w')

# Запись данных в базу(файл)
    elif choice == '2':
        open_base()
        print('\nВводите новые записи формата "страна климат почва,',end='')
        print('разделяя их переносом строки.')
        print('Ввод пустой строки завершит запись.')
        new_record = input()
        while new_record != '':
            new_record = new_record.rsplit(' ',2)
            new_record = new_record[0]+';'+new_record[1]+';'+new_record[2]+'\n'
            base.write(new_record)
            new_record = input()

            
# Поиск по базе(файлу)
    elif choice == '3':
        open_base()
        base.seek(0)
        seek_row = 4
        print('Поиск по базе (ab) или  столбцу (col)?')
        if input() != 'ab':
            print('Выберите номер столбца: ')
            print('0 - страна','\n1 - климат','\n2 - почва')

            
            # Поиск по столбцу: запрос
            seek_row = int(input())
        seek_full = False
        if seek_row != 4:
            print('Нужен точный результат?(y/n)')
            if input() == 'y': seek_full = True
        phrase = input('Введите искомое значение: ')
        print('\nРезультаты:')
        counter = 0
        result_table = open('Results.csv','w')  # Файл для записи результата
        # Сам поиск
        for i in base.readlines():
            if seek_row != 4:
                record = i[:-1].split(';')[seek_row]
            else:
                record = i[:-1].replace(';',' ')
            if seek_full and phrase == record or\
            (not seek_full and phrase in record):
                print(i[:-1].replace(';','\t'))
                result_table.write(i)
                counter += 1
        result_table.close()
        print('\nНайдено записей:',counter)

        
# Вывод данных из файла 
    elif choice == '4':
        open_base()
        print('Страна',' '*17,'Климат',' '*17,'Почва')
        print()
        base.seek(0)
        for i in base.readlines():
            print(i.replace(';','\t\t',2),end='')

    elif choice == '0':
        if base_open:
            base.close()
        print('Выход')
        break
# Обработка ошибок
    else:
        print('Неверный выбор! Введите цифру от 0 до 4:')
        continue
    print('\n'+'='*77+'\n')
