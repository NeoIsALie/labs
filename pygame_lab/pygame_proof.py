# pygame proof
import pygame, time
import sys
import math

pygame.init()
# размеры окна
width = 600
height = 480

# цвета
skyblue = (135,206,235)
steelblue = (70,130,180)
white = (255,255,255)
black = (0,0,0)

screen = pygame.display.set_mode((width, height),0,32)
pygame.display.set_caption('destroy everything')
screen.fill(skyblue)

sin_width = 150
sin_height = 100
step = 1

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit(0)

    screen.fill(skyblue)
    print(step)
    # волны
    pygame.draw.arc(screen,white,(0,300,150,100),0,math.pi)
    pygame.draw.arc(screen,white,(150,300,150,100),-math.pi,0)
    pygame.draw.arc(screen,white,(300,300,150,100),0,math.pi)
    pygame.draw.arc(screen,white,(450,300,150,100),-math.pi,0)

    # лодка
    if step == 0:
        pygame.draw.arc(screen,black,(50,270,50,30),-math.pi,0)
        pygame.draw.line(screen,black,(50,285),(100,285))
        pygame.draw.line(screen,black,(75,285),(75,250))
        pygame.draw.line(screen,black,(75,250),(90,270))
        pygame.draw.line(screen,black,(90,270),(75,270))
        pygame.display.update()

    if step == 1:
        pygame.draw.arc(screen,black,(145,315,50,30),math.pi/2,-math.pi/2)
        pygame.draw.line(screen,black,(172,315),(172,345))
        pygame.draw.line(screen,black,(172,330),(182,330))
        
    time.sleep(1)
    pygame.display.update()

    
