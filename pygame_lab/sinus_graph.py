#draw a sine wave

import pygame
import time
import math
import sys



canvas_width = 640
canvas_height = 480


color = pygame.Color(255, 255, 0, 0)
background_color = pygame.Color(0, 0, 0, 0)


pygame.init()

pygame.display.set_caption("Sine Wave")


screen = pygame.display.set_mode((canvas_width, canvas_height))
screen.fill(background_color)


surface = pygame.Surface((canvas_width, canvas_height))
surface.fill(background_color)




while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    surface.fill(background_color)



    frequency = 4
    amplitude = 50 
    speed = 1
    for x in range(0, canvas_width):
        y = int((canvas_height/2) +
                amplitude*math.sin(frequency*
                                   ((float(x)/canvas_width)*(2*math.pi)+
                                    (speed*time.time()))))
        surface.set_at((x, y), color)

    screen.blit(surface, (0, 0))

    pygame.display.flip()








SCREEN_WIDTH = 400
SCREEN_HEIGHT = 400
DISPLAY = (SCREEN_WIDTH,SCREEN_HEIGHT)		
											 
pygame.init()	
screen = pygame.display.set_mode(DISPLAY)	
pygame.display.set_caption('Пример')		


done = False

box_WIDTH = 50                  
box_HEIGHT = 75               

x = (SCREEN_WIDTH-box_WIDTH)//2
y = (SCREEN_HEIGHT-box_HEIGHT)//2


while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True  

    bg = Surface(DISPLAY)	    
    bg.fill(Color("lightblue"))     
    screen.blit(bg, (0,0))      

    box = Surface( (box_WIDTH, box_HEIGHT) )    
                                                
    TRANSPARENT_COLOR = Color("#123456")   
    box.set_colorkey(TRANSPARENT_COLOR)    
    box.fill(TRANSPARENT_COLOR)
    
    pygame.draw.rect(box, Color("red"), Rect(0, 25, 50, 50), 0)
    pygame.draw.circle(box, Color("blue"), (25, 50), 10, 0) 
    screen.blit(box, (x,y) )
    pygame.draw.line(screen, Color("black"), (0, (SCREEN_HEIGHT+box_HEIGHT)//2),
                     (SCREEN_WIDTH, (SCREEN_HEIGHT+box_HEIGHT)//2), 5)
    pygame.draw.ellipse(screen, Color("yellow"),
                        Rect(SCREEN_WIDTH//2,50, 100,50), 0)
    pygame.draw.arc(screen, Color("brown"),
                    Rect(SCREEN_WIDTH-150,50, 100, 100), 3*pi/2, 2*pi, 3)
    pygame.draw.lines(screen, Color("black"), True,
                      ( (30,30), (50,50), (50, 60), (90, 80), (120, 50)),2)
    pygame.display.update()

pygame.quit()           




