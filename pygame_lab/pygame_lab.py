# Покасова Анастасия, ИУ7-21
# pygame: машина для сноса зданий и бегущие люди

import pygame
import time
import sys
import math

#Спица
def draw_luchi(screen,x1,y1,x2,y2,x3,y3,x4,y4):
    grey = (155,155,155)
    polygon_luchi_points = [(x1,y1),(x3,y3),(x4,y4),(x2,y2)]
    pygame.draw.polygon(screen,grey, polygon_luchi_points, 0)

#Поворот точки
def rotate(x_1,y_1):
    x_11 = x_1
    x_1 = x_center + (x_1 - x_center) * math.cos(0.09) + (
      (y_1 - y_center) * math.sin(0.09))
    y_1 = y_center + (y_1 - y_center) * math.cos(0.09) - (
      (x_11 - x_center) * math.sin(0.09))
    return x_1,y_1

def draw_town(t1,t2,t3,t4,t5,t6,t7,t8,t9,t10):
    # Город на фоне
    pygame.draw.rect(screen, grey,(t1,240,50,160),0)
    pygame.draw.rect(screen, grey,(t2,350,25,50),0)
    pygame.draw.rect(screen, grey,(t3,145,45,255),0)
    pygame.draw.rect(screen, grey,(t4,310,30,90),0)
    pygame.draw.rect(screen, grey,(t5,185,55,215),0)
    pygame.draw.rect(screen, grey,(t6,290,20,110),0)
    pygame.draw.rect(screen, grey,(t7,140,45,260),0)
    pygame.draw.rect(screen, seagreen,(t8,85,25,265),0)
    pygame.draw.rect(screen, darkgrey,(t9,175,25,135),0)
    pygame.draw.rect(screen, darkgrey,(t10,130,20,190),0)
    
def draw_building(w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11):
    # Главное здание
    pygame.draw.rect(screen,white,(w1,150,120,250),0)
    pygame.draw.rect(screen,steelblue,(w2,160,40,40),0)
    pygame.draw.rect(screen,steelblue,(w3,160,40,40),0)
    pygame.draw.rect(screen,steelblue,(w4,210,40,40),0)
    pygame.draw.rect(screen,steelblue,(w5,210,40,40),0)
    pygame.draw.rect(screen,steelblue,(w6,260,40,40),0)
    pygame.draw.rect(screen,steelblue,(w7,260,40,40),0)
    pygame.draw.rect(screen,steelblue,(w8,310,40,40),0) 
    pygame.draw.rect(screen,steelblue,(w9,310,40,40),0)
    pygame.draw.rect(screen,steelblue,(w10,360,40,20),0) 
    pygame.draw.rect(screen,steelblue,(w11,360,40,20),0)

def draw_machine():
    # Кран
    pygame.draw.polygon(screen,darkorange,((p_x_1,180),(p_x_2,250),
                        (p_x_3,320),(p_x_4,260)),3)
    pygame.draw.line(screen,black,(l_x,180),(l_x,260),2)
    pygame.draw.circle(screen,darkgrey,(ball_x,280),20,0)
    # Кабина
    pygame.draw.rect(screen,darkorange,[cab_x_1,275,60,45],0)
    pygame.draw.rect(screen,darkorange,[cab_x_2,320,130,50],0)
    pygame.draw.rect(screen,black,[cab_x_3,280,30,20],0)
    pygame.draw.rect(screen,grey,[cab_x_4,370,105,28],0)
    # Колеса
    pygame.draw.ellipse(screen,black,(wh_x_1,370,36,36))
    pygame.draw.ellipse(screen,black,(wh_x_2,370,36,36))
    
    

pygame.init()

# rgb цвета
darkgrey = (169,169,169)
grey = (128,128,128)
dimgrey = (105, 105, 105)
seagreen = (143,188,143)
skyblue = (135,206,235)
steelblue = (70,130,180)
white = (255, 255, 255)
darkorange = (255,140,0)
black = (0,0,0)

screen = pygame.display.set_mode((960,480),0,32)
pygame.display.set_caption('destroy everything')
screen.fill(skyblue)

# Координаты бульдозера
p_x_1,p_x_2,p_x_3,p_x_4 = 765, 810, 820, 785
l_x = 765
ball_x = 765
cab_x_1,cab_x_2,cab_x_3,cab_x_4 = 810,790,840,805
wh_x_1,wh_x_2 = 790,890

t1,t2,t3,t4,t5 = 0,50,80,125,155                # город
t6,t7,t8,t9,t10 = 230,250,50,130,230            
w1,w2,w3,w4,w5 = 415,430,480,430,480            # здание
w6,w7,w8,w9,w10,w11 = 430,480,430,480,430,480   

# координаты людей
#1
head_1_x, head_1_y = 410, 375
b_1_x1, b_1_y1, b_1_x2, b_1_y2 = 415, 385, 415, 395
la_1_x1, la_1_y1, la_1_x2, la_1_y2 = 415, 385, 412, 395
ra_1_x1, ra_1_y1, ra_1_x2, ra_1_y2 = 415, 385, 417, 395
ll_1_x1, ll_1_y1, ll_1_x2, ll_1_y2 = 415, 395, 412, 405
rl_1_x1, rl_1_y1, rl_1_x2, rl_1_y2 = 415, 395, 417, 405
#2
head_2_x, head_2_y = 420, 400
b_2_x1, b_2_y1, b_2_x2, b_2_y2 = 425, 410, 425, 420
la_2_x1, la_2_y1, la_2_x2, la_2_y2 = 425, 410, 422, 420
ra_2_x1, ra_2_y1, ra_2_x2, ra_2_y2 = 425, 410, 427, 420
ll_2_x1, ll_2_y1, ll_2_x2, ll_2_y2 = 425, 420, 422, 430
rl_2_x1, rl_2_y1, rl_2_x2, rl_2_y2 = 425, 420, 427, 430
#3
head_3_x, head_3_y = 425, 375
b_3_x1, b_3_y1, b_3_x2, b_3_y2 = 430, 385, 430, 395
la_3_x1, la_3_y1, la_3_x2, la_3_y2 = 430, 385, 427, 395
ra_3_x1, ra_3_y1, ra_3_x2, ra_3_y2 = 430, 385, 433, 395
ll_3_x1, ll_3_y1, ll_3_x2, ll_3_y2 = 430, 395, 427, 405
rl_3_x1, rl_3_y1, rl_3_x2, rl_3_y2 = 430, 395, 433, 405

# точки центра (для вращения)
x_center = 808
y_center = 390

# спицы на первом колесе
x_1 = x_center+14
y_1 = y_center+2
x_2 = x_center+14
y_2 = y_center-2
x_3 = x_center-14
y_3 = y_center+2
x_4 = x_center-14
y_4 = y_center-2

# спицы на втором колесе
x_5 = x_center+2
y_5 = y_center+14
x_6 = x_center+2
y_6 = y_center-14
x_7 = x_center-2
y_7 = y_center+14
x_8 = x_center-2
y_8 = y_center-14

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit(0)
    screen.fill(skyblue)
    time.sleep(0.001)
    
    # отрисовка фона
    pygame.draw.rect(screen,dimgrey,(0,400,960,80),0)
    draw_town(t1,t2,t3,t4,t5,t6,t7,t8,t9,t10)
    # отрисовка здания и машины
    draw_building(w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11)
    draw_machine()
    
    # Бегущие люди
    #1
    pygame.draw.ellipse(screen,white,(head_1_x,head_1_y,10,10))
    pygame.draw.line(screen,white,(b_1_x1, b_1_y1),(b_1_x2,b_1_y2))
    pygame.draw.line(screen,white,(la_1_x1, la_1_y1),(la_1_x2, la_1_y2))
    pygame.draw.line(screen,white,(ra_1_x1, ra_1_y1),(ra_1_x2, ra_1_y2))
    pygame.draw.line(screen,white,(ll_1_x1, ll_1_y1),(ll_1_x2, ll_1_y2))
    pygame.draw.line(screen,white,(rl_1_x1, rl_1_y1),(rl_1_x2, rl_1_y2))
    
    #2
    pygame.draw.ellipse(screen,white,(head_2_x,head_2_y,10,10))
    pygame.draw.line(screen,white,(b_2_x1, b_2_y1),(b_2_x2, b_2_y2))
    pygame.draw.line(screen,white,(la_2_x1, la_2_y1), (la_2_x2, la_2_y2))
    pygame.draw.line(screen,white,(ra_2_x1, ra_2_y1), (ra_2_x2, ra_2_y2))
    pygame.draw.line(screen,white,(ll_2_x1, ll_2_y1), (ll_2_x2, ll_2_y2))
    pygame.draw.line(screen,white,(rl_2_x1, rl_2_y1), (rl_2_x2, rl_2_y2))

    #3
    pygame.draw.ellipse(screen,white,(head_3_x,head_3_y,10,10))
    pygame.draw.line(screen,white,(b_3_x1, b_3_y1),(b_3_x2,b_3_y2))
    pygame.draw.line(screen,white,(la_3_x1, la_3_y1),(la_3_x2, la_3_y2))
    pygame.draw.line(screen,white,(ra_3_x1, ra_3_y1),(ra_3_x2, ra_3_y2))
    pygame.draw.line(screen,white,(ll_3_x1, ll_3_y1),(ll_3_x2, ll_3_y2))
    pygame.draw.line(screen,white,(rl_3_x1, rl_3_y1),(rl_3_x2, rl_3_y2))
    
    #Изменение координат одной спицы        
    x_1,y_1 = rotate(x_1,y_1)
    x_2,y_2 = rotate(x_2,y_2)
    x_3,y_3 = rotate(x_3,y_3)
    x_4,y_4 = rotate(x_4,y_4)
    
    #Изменение координат второй спицы 
    x_5,y_5 = rotate(x_5,y_5)
    x_6,y_6 = rotate(x_6,y_6)
    x_7,y_7 = rotate(x_7,y_7)
    x_8,y_8 = rotate(x_8,y_8)
        
        
        
    #Первое колесо
    draw_luchi(screen, int(x_1), int(y_1), int(x_2), int(y_2),
           int(x_3), int(y_3), int(x_4), int(y_4))
    draw_luchi(screen, int(x_5), int(y_5), int(x_6), int(y_6),
               int(x_7), int(y_7), int(x_8), int(y_8))

   #Второе колесо
    draw_luchi(screen, int(x_1 + 100), int(y_1),int(x_2+100),
               int(y_2), int(x_3 + 100), int(y_3), int(x_4 + 100), int(y_4))
    draw_luchi(screen, int(x_5 + 100), int(y_5), int(x_6 + 100), int(y_6),
               int(x_7 + 100), int(y_7), int(x_8 + 100), int(y_8))
    
    # Изменение координат
    # город
    w1 += 1
    w2 += 1
    w3 += 1
    w4 += 1
    w5 += 1
    w6 += 1
    w7 += 1
    w8 += 1
    w9 += 1
    w10 += 1
    w11 += 1
    
    # главное здание
    t1 += 1
    t2 += 1
    t3 += 1
    t4 += 1
    t5 += 1
    t6 += 1
    t7 += 1
    t8 += 1
    t9 += 1
    t10 += 1
         
    # люди
    #1
    head_1_x -= 1
    b_1_x1 -= 1
    b_1_x2 -=1 
    la_1_x1 -= 1
    la_1_x2 -= 1
    ra_1_x1 -= 1
    ra_1_x2 -= 1
    ll_1_x1 -= 1
    ll_1_x2 -= 1
    rl_1_x1 -= 1
    rl_1_x2 -= 1
    #2
    head_2_y += 1
    b_2_y1 += 1
    b_2_y2 += 1
    la_2_y1 += 1
    la_2_y2 += 1
    ra_2_y1 += 1
    ra_2_y2 += 1
    ll_2_y1 += 1
    ll_2_y2 += 1
    rl_2_y1 += 1
    rl_2_y2 += 1
    #3
    head_3_x -= 1    
    b_3_x1 -= 1
    b_3_x2 -= 1 
    la_3_x1 -= 1
    la_3_x2 -= 1
    ra_3_x1 -= 1
    ra_3_x2 -= 1
    ll_3_x1 -= 1
    ll_3_x2 -= 1
    rl_3_x1 -= 1
    rl_3_x2 -= 1
    
    time.sleep(0.01)
    pygame.display.update()
    
