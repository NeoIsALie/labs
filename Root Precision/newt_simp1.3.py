# Покасова Анастасия, ИУ7-21.
# Уточнение корней функции упрощенным методом Ньютона.
# Вывод графика функции с точками min/max, перегиба.


import matplotlib.pyplot as plt
import numpy as np
from math import sin, cos

legend = '''
0 - нет ошибки
-1 - ошибка сходимости
'''

# функция и её производные
def f(x):
    return np.sin(x)#4*x*x*x - 40*x
def derivative(x):
    return cos(x)#12*x*x - 40
def derivative_2(x):
    return -sin(x)#24*x

# метод уточнения
def n_simp(x0, eps, maxiter):
    iteration = 1
    x_tmp = 0
    xn = 0
    x1 = x0 - f(x0)/derivative(x0)
    while abs(xn-x1) >= eps and iteration < n_max:
        x_tmp = x1
        xn = x1 - f(x1)/derivative(x0)
        x1 = xn
        iteration+=1
    if iteration >= n_max:
        code = '-1'
    else:
        code = '0'
    return x_tmp, xn, iteration, code

# поиск максимума/минимума функции
def pltf_min_max(a,b,h):
    min_x = [a]
    max_x = [a]
    x = a+h
    while x<b:
        if ((round(derivative(x)) == 0)
            and (f(x-h) > f(x) and f(x) < f(x+h))):
                min_x+=[x]
        elif ((round(derivative(x)) == 0)
            and (f(x-h) < f(x) and f(x) > f(x+h))):
                max_x+=[x]
        x+=h
    min_x.remove(a)
    max_x.remove(a)
    return min_x, max_x

# поиск точек перегиба
def pltfind_curve(a,b,h,eps):
    curves = []
    x=a+h
    while x<b:
        if derivative_2(x-h) * derivative_2(x+h) < 0:
            curves+=[x]
        x+=h
    return curves
 
t_h1 = '| № |   Xn   |  Xn+1  |   X    |'
t_h2 = '  F(x)   |Итераций|Код ошибки|'

# получение исходных данных
print('Введите концы отрезка исследования,',end='')
print('шаг,точность через запятую')
a,b,h,eps = map(float,input().split(','))
print('Введите максимальное число итераций')
n_max = int(input())

rbs = []  # начальные точки приближения
plroot_x = []  # абсциссы корней

# определение min/max и перегиба
min_x, max_x = pltf_min_max(a,b,h/100)
curves = pltfind_curve(a,b,h/100, 0.001)

# определение начальных точек приближения
x = a
while x < b:
    if (f(x) < 0) and (f(x+h) > 0):
        rb = x+h
        rbs+=[rb]
    elif (f(x) > 0) and (f(x+h) < 0):
        rb = x+h
        rbs+=[rb]
    x+=h

# вывод результатов 
if len(rbs) == 0:
    print('Нет корней') 
else:
    print(legend)
    print()
    print('-'*62)
    print(t_h1+t_h2)
    print('-'*62)
    for r in rbs:
        xn, xn1, iteration, code = n_simp(r, eps, n_max)
        plroot_x += [xn1]
        print('|{:3d}|{:8.5f}|{:8.5f}|{:8.5f}|\
{:>9.2e}|{:8d}|{:>10s}|'.format(rbs.index(r)+1,
              xn, xn1,xn1,f(xn1),
              iteration,code))
    print('-'*62)


# вывод графика функции
plt.title("$sin(x)$")
plt.xlabel("x")
plt.ylabel("y")
x = np.arange(a,b,h)
y = f(x)
plt.plot(x,y,label="Graph")
plt.grid()
for i in range(len(min_x)):
        if i == 0:
            plt.plot(min_x[i], f(min_x[i]), 'co', label="Min")
        else:
            plt.plot(min_x[i], f(min_x[i]), 'co')
for i in range(len(max_x)):
        if i == 0:
            plt.plot(max_x[i], f(max_x[i]), 'ro', label="Max")
        else:
            plt.plot(max_x[i], f(max_x[i]), 'ro')
for i in range(len(curves)):
        if i == 0:
            plt.plot(curves[i], f(curves[i]), 'ko', label="Curve")
        else:
            plt.plot(curves[i], f(curves[i]), 'ko')
for i in range(len(plroot_x)):
        if i == 0:
            plt.plot(plroot_x[i], f(plroot_x[i]), 'go',label="Root")
        else:
            plt.plot(plroot_x[i], f(plroot_x[i]), 'go')
plt.legend()
plt.show()
        
        
