import matplotlib.pyplot as plt
import numpy as np

def f(x):
    return (x-1)*(x-1)*(x-1)+1
def derivative(x):
    return 3*(x-1)*(x-1)
def secant(x0,x1,eps):
    x1 = x0+eps
    iteration = 1
    code = '0'
    while True:
        xn = x1-(f(x1)*(x1-x0)/(f(x1)-f(x0)))
        if iteration>n_max:
            code = '-1'
        if abs(xn-x1) < eps:
            return xn, iteration, code
        x0=x1
        x1=xn
            
print('Введите концы отрезка исследования,',end='')
print('шаг,точность через запятую')
a,b,h,eps = map(float,input().split(','))
print('Введите максимальное число итераций')
n_max = int(input())
rbs = []
plroot_x = []

x = a
while x < b:
    if (f(x) < 0) and (f(x+h) > 0):
        rb = x+h
        rbs+=[rb]
    elif (f(x) > 0) and (f(x+h) < 0):
        rb = x+h
        rbs+=[rb]
    x+=h
t_h1 = '| № |  Xn+1  |   X    |'
t_h2 = '  F(x)   |Итераций|Код ошибки|'

if len(rbs) == 0:
    print('Нет корней') 
else:
    print()
    print('-'*56)
    print(t_h1+t_h2)
    print('-'*56)
    for r in rbs:
        xn1, iteration, code = secant(r, eps, n_max)
        plroot_x += [xn1]
        print('|{:3d}|{:8.5f}|{:8.5f}|\
{:>9.2e}|{:8d}|{:>10s}|'.format(rbs.index(r)+1,
               xn1,xn1,f(xn1),
              iteration,code))
    print('-'*56)



x = np.arange(a,b,h)
y = f(x)
plt.plot(x,y,label="Graph")
plt.grid()
for i in range(len(plroot_x)):
        if i == 0:
            plt.plot(plroot_x[i], f(plroot_x[i]), 'go',label="Root")
        else:
            plt.plot(plroot_x[i], f(plroot_x[i]), 'go')
plt.legend()
plt.show()
